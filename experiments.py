import optimisation
import numpy as np
import matplotlib.pyplot as plt
import rayPlotting


def planoconvexorientation():
    """
    Function to plot the optimal root mean square radius spot size of planoconvex lenses of various radii of curvatures.
    Tests both planoconvex lenses in both orientations, ie. planar side facing towards and away from incoming light.
    """
    bundlerad = 1.0
    focallengths = []
    focallengths2 = []

    ### Planoconvex lens with planar lens facing away from incoming beam
    rmsr1 = []
    for rad in np.arange(10,50,1):
        rads = (rad,0)
        focallength = optimisation.findfocus(rads[0],rads[1], 70, bundlerad = bundlerad)[0][0]
        focallengths += [focallength]
        rmsr1 += [optimisation.calcRMSR(optimisation.runExp(focallength, rads, bundlerad))]


    ### Binconvex lens optimised to same focal length as planoconvex lens above
    rmsr2 = []
    for focallength in focallengths:
        rads = (20,-20)
        radii = optimisation.optimisecurvatures(rads[0], rads[1], focallength)[0]
        rads = (radii[0], radii[1])
        rmsr2 += [optimisation.calcRMSR(optimisation.runExp(focallength, rads, bundlerad))]

    ### Planoconvex lens with planar lens facing towards incoming beam
    rmsr3 = []
    for rad in np.arange(10,50,1):
        rads = (0,-1 * rad)
        focallength = optimisation.findfocus(rads[0],rads[1], 50, bundlerad = bundlerad)[0][0]
        focallengths2 += [focallength]
        rmsr3 += [optimisation.calcRMSR(optimisation.runExp(focallength, rads, bundlerad))]

    fig = plt.figure()
    plt.plot(focallengths,rmsr1, "r", focallengths,rmsr2, "g",focallengths2,rmsr3, "b")
    #plt.plot()
    #plt.plot()
    plt.show()

def planoconvex():
    """
    """
    rads = (10,0)
    focallength = optimisation.findfocus(rads[0],rads[1], 70)[0][0]
    rmsr1 = []
    for radius in np.arange(0.1,10,0.1):
        rmsr1 += [optimisation.calcRMSR(optimisation.runExp(focallength, rads, radius))]

    rads = (10,-10)
    radii = optimisation.optimisecurvatures(rads[0], rads[1], focallength)[0]
    print radii
    rads = (radii[0], radii[1])
    rmsr2 = []
    for radius in np.arange(0.1,10,0.1):
        rmsr2 += [optimisation.calcRMSR(optimisation.runExp(focallength, rads, radius))]
    
    fig = plt.figure()
    plt.plot(np.arange(0.1,10,0.1), rmsr1, "r")
    plt.plot(np.arange(0.1,10,0.1), rmsr2, "b")
    #plt.plot(np.arange(0.1,10,0.1),(588*10**-9)*(focallength-15)/np.arange(0.1,10,0.1))
    plt.show()

def optimisedlens():
    global focallength
    rads = (10,-10)
    radii = optimisation.optimisecurvatures(rads[0], rads[1], focallength)[0]
    rads = (radii[0], radii[1])
    print rads
    rsmr = []
    for radius in np.arange(0.1,10,0.1):
        rsm = optimisation.calcRMSR(optimisation.runExp(focallength, rads, radius))
        rsmr += [rsm]


    fig = plt.figure()
    plt.plot(np.arange(0.1,10,0.1), rsmr)
    plt.plot(np.arange(0.1,10,0.1),(588*10**-9)*(focallength-15)/np.arange(0.1,10,0.1))
    plt.show()
