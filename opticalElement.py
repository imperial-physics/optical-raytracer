import numpy as np
import raytracer

class OpticalElement():
    """
    Parent class of more specific optical elements such as:
        SphericalRefraction
        PlanarRefraction
        OpticalPlane
    """
    
    elements = []

    def intercept(self):
        return self.__intercept

    def normalVector(self):
        return self.__normalVector

    def normalise(self,vector):
        """
        Normalises a vector
        """
        magnitude = (np.dot(vector,vector))**0.5
        if magnitude == 0:
            return np.array(vector)
        else:
            return np.array(vector)/magnitude

    def refraction(self, ray):
        """
        Method to refract a ray passing though the surface and return the resulting direction vector

        Accomplishes this by calculating the angle from the plane's normal to the refracted ray,
        then rotating the normal vector around the axis formed by the cross product of the incident ray's direction vector and the normal vector.
        """

        if np.array_equal(self.normalVector, ray.direction()): ##Special case for if ray's direction
            return ray.direction()                             ##is parallel to normal vector

        costheta1 = np.dot(self.normalVector(), ray.direction())
        sintheta1 = (1 - costheta1**2)**0.5

        if sintheta1 > self.n2()/self.n1():
            print "Total Internal Reflection"
            return self.reflection(ray)

        sintheta2 = self.n1()/self.n2() * sintheta1
        costheta2 = (1 - sintheta2**2)**0.5

        rotaxis = self.normalise(np.cross(self.normalVector(), ray.direction()))

        rotationarray = np.array((
            [costheta2 + (1 - costheta2) * rotaxis[0]**2, rotaxis[0] * rotaxis[1] * (1-costheta2) - rotaxis[2] * sintheta2, rotaxis[0] * rotaxis[2] * (1 - costheta2) + rotaxis[1] * sintheta2],
            [rotaxis[0] * rotaxis[1] * (1 - costheta2) + rotaxis[2] * sintheta2, costheta2 + (1 - costheta2) * rotaxis[1]**2, rotaxis[1]*rotaxis[2] * (1 - costheta2) - rotaxis[0] * sintheta2],
            [rotaxis[0] * rotaxis[2] * (1 - costheta2) - rotaxis[1] * sintheta2, rotaxis[1] * rotaxis[2]*(1 - costheta2) + rotaxis[0] * sintheta2, costheta2 + (1 - costheta2) * rotaxis[2]**2]))
        

        newdirection = np.dot((rotationarray), self.normalVector())
        return newdirection

    def reflection(self, ray):
        """
        Reflects a ray of light
        """
        if np.array_equal(self.normalVector, ray.direction()): ##Special case for if ray's direction
            return -1 * ray.direction()                        ##is parallel to normal vector

        costheta = np.dot(self.normalVector(), ray.direction())
        theta = np.arccos(costheta)
        alpha = np.pi - 2 * theta
        cosalpha = np.cos(alpha)
        sinalpha = np.sin(alpha)

        rotaxis = self.normalise(np.cross(self.normalVector(), ray.direction()))

        rotationarray = np.array((
            [cosalpha + (1 - cosalpha) * rotaxis[0]**2, rotaxis[0] * rotaxis[1] * (1-cosalpha) - rotaxis[2] * sinalpha, rotaxis[0] * rotaxis[2] * (1 - cosalpha) + rotaxis[1] * sinalpha],
            [rotaxis[0] * rotaxis[1] * (1 - cosalpha) + rotaxis[2] * sinalpha, cosalpha + (1 - cosalpha) * rotaxis[1]**2, rotaxis[1]*rotaxis[2] * (1 - cosalpha) - rotaxis[0] * sinalpha],
            [rotaxis[0] * rotaxis[2] * (1 - cosalpha) - rotaxis[1] * sinalpha, rotaxis[1] * rotaxis[2]*(1 - cosalpha) + rotaxis[0] * sinalpha, cosalpha + (1 - cosalpha) * rotaxis[2]**2]))

        newdirection = np.dot((rotationarray), self.normalVector())
        return newdirection

    def propagateRay(self, ray):
        raise NotImplementedError()


class SphericalRefraction(OpticalElement):
    """
    Class describing a spherical surface
    """

    def __init__(self, zCut, radCurvature, radAperture, n1, n2):
        """
        Initialises a spherical refractive surface centred on the z axis.
        Note: to implement a concave surface, input a negative radius of curvature.
        """
        self.__zCut = zCut 
        self.__centre = zCut + radCurvature
        self.__radCurvature = radCurvature
        self.__radAperture = radAperture
        self.__n1 = n1
        self.__n2 = n2
        OpticalElement.elements.append(self) ## Every opticalElement is added to a list of opticalElements

    def radCurvature(self):
        return self.__radCurvature

    def aperture(self):
        return self.__radAperture

    def intercept(self):
        return self.__intercept

    def normalVector(self):
        return self.__normalVector

    def n1(self):
        return self.__n1

    def n2(self):
        return self.__n2

    def calcIntercept(self, ray):
    	"""
    	Method to find the position at which a given ray intersects with a spherical surface
    	"""

        ### Can't handle the ray missing the surface entirely yet, only if it would hit but aperture is too small
    	rvector = ray.position() - np.array([0,0,self.__centre])
        if self.radCurvature() > 0:
            lConvex = - np.dot(rvector, ray.direction()) - ((np.dot(rvector, ray.direction()))**2 - (np.dot(rvector, rvector) - self.__radCurvature**2))**0.5
            if lConvex < 0:
                raise Exception("Surface is behind light ray. You may have ordered your surfaces incorrectly.")
            intercept = ray.position() + lConvex * ray.direction()

        elif self.radCurvature() < 0:

            ## Special case if ray starts from centre of curvature, if so direction vector to surface has magnitude of radius of curvature.
            ## This tended to break the code for some reason. Can possibly be removed in future.
            if np.dot(rvector,rvector) == 0:
                lConcave = abs(self.radCurvature())
            else:
                lConcave = - np.dot(rvector, ray.direction()) + ((np.dot(rvector, ray.direction()))**2 - (np.dot(rvector, rvector) - self.__radCurvature**2))**0.5
            if lConcave < 0:
                raise Exception("Surface is behind light ray. You may have ordered your surfaces incorrectly.")
            intercept = ray.position() + lConcave * ray.direction()

        if (intercept[0]**2 +intercept[1]**2)**0.5 < self.aperture():
            self.__intercept = intercept
        else: 
            return 0

    def setNormalVector(self):
        if self.radCurvature() > 0:
            self.__normalVector = self.normalise(np.array([0,0,self.__centre]) - self.intercept())
        elif self.radCurvature() < 0:
            self.__normalVector = self.normalise(self.intercept() - np.array([0,0,self.__centre]))

    def propagateRay(self, ray):
        """
        propagates a ray
        """
        if self.calcIntercept(ray) != 0:
            self.setNormalVector()
            ray.append(self.intercept(),self.refraction(ray))

    def propagateBundle(self, bundle):
        """
        Propagates a bundle of rays
        """
        for ray in bundle.rays():
            self.propagateRay(ray)


class PlanarRefraction(OpticalElement):
    """
    Class describing a planar surface
    """

    def __init__(self, zCut, normalVector, n1, n2):
        """
        Initialises a planar refractive surface.

        Note: zCut is the point on the z zxis which intersects the plane
        """
        self.__zCut = zCut
        self.__normalVector = self.normalise(normalVector)
        self.__n1 = n1
        self.__n2 = n2
        OpticalElement.elements.append(self) ## Every opticalElement is added to a list of opticalElements)


    def intercept(self):
        return self.__intercept

    def calcIntercept(self, ray):
        """
        Method to find the position at which a given ray intersects with a planar surface

        NOTE: Currently only deals with planes that lie in the x-y plane, could be extended
        """
        rvector = ray.position() - [0,0,self.__zCut]
        l = - np.dot(rvector, [0,0,1])/np.dot(ray.direction(), [0,0,1])
        intercept = ray.position() + l * ray.direction()
        self.__intercept = intercept

    def propagateRay(self, ray):
        """
        Calculates the position of the ray as it hits a non-refracting plane
        """
        self.calcIntercept(ray)
        ray.append(self.intercept(), ray.direction())
    
    def propagateBundle(self, bundle):
        """
        Propagates a bundle of rays
        """
        for ray in bundle.rays():
            self.propagateRay(ray)

class OpticalPlane(OpticalElement):
    """
    A plane defining the observation plane on which the light pattern is observed.
    """

    def __init__(self, zCut, normalVector = [0,0,1]):
        """
        Initialises a planar refractive surface.

        Note: zCut is the point on the z zxis which intersects the plane
        """
        self.__zCut = zCut
        self.__normalVector = self.normalise(normalVector)
        OpticalElement.elements.append(self) ## Every opticalElement is added to a list of opticalElements)


    def refraction(self,ray):
        raise NotImplementedError()

    def intercept(self):
        return self.__intercept

    def calcIntercept(self, ray):
        """
        Method to find the position at which a given ray intersects with a planar surface

        NOTE: Currently only deals with planes that lie in the x-y plane, could be extended
        """

        rvector = ray.position() - np.array([0,0,self.__zCut])
        l = - np.dot(rvector, [0,0,1])/np.dot(ray.direction(), [0,0,1])
        intercept = ray.position() + l * ray.direction()
        self.__intercept = intercept

    def propagateRay(self, ray):
        """
        Calculates the position of the ray as it hits a non-refracting plane
        """
        self.calcIntercept(ray)
        ray.append(self.intercept(), ray.direction())
    
    def propagateBundle(self, bundle):
        """
        Propagates a bundle of rays
        """
        for ray in bundle.rays():
            self.propagateRay(ray)

def propagate(light):
    """
    Automatically propagates the entered object (Ray or Bundle) through all
    OpticalElements.
    """
    if isinstance(light, raytracer.Ray):
        for element in OpticalElement.elements:
            element.propagateRay(light)
            
    elif isinstance(light, raytracer.Bundle):
        for element in OpticalElement.elements:
            element.propagateBundle(light)
