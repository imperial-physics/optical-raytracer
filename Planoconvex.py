import optimisation
import numpy as np
import matplotlib.pyplot as plt
import rayPlotting

focallength = 0

def planoconvex():
    global focallength
    rads = (0,-10)
    focallength = optimisation.findfocus(rads[0],rads[1], 70)[0][0]
    rmsr = []
    #for radius in np.arange(0.1,10,0.1):
    radius = 0.5
    rms = optimisation.calcRMSR(optimisation.runExp(focallength, rads, radius))
    rmsr += [rms]


    #fig = plt.figure()
    #plt.plot(np.arange(0.1,10,0.1), rsmr)
    #plt.plot(np.arange(0.1,10,0.1),(588*10**-9)*(focallength-15)/np.arange(0.1,10,0.1))
    print rms
    plt.show()

def optimisedlens():
    global focallength
    rads = (10,-10)
    radii = optimisation.optimisecurvatures(rads[0], rads[1], focallength)[0]
    rads = (radii[0], radii[1])
    print rads
    rsmr = []
    for radius in np.arange(0.1,10,0.1):
        rsm = optimisation.calcRMSR(optimisation.runExp(focallength, rads, radius))
        rsmr += [rsm]


    fig = plt.figure()
    plt.plot(np.arange(0.1,10,0.1), rsmr)
    plt.plot(np.arange(0.1,10,0.1),(588*10**-9)*(focallength-15)/np.arange(0.1,10,0.1))
    plt.show()
