"""
Raytracer.py
Tom French 22/10/15
"""

import numpy as np
import opticalElement
import rayPlotting
import matplotlib

class Ray():
    """
    Class defining a optical ray

    Inputs:
        startPos: A 3 dimensional cartesian position vector describing the inital position of the ray
        direction: a 3 dimensional cartesian vector describing the initial direction of the ray

    """

    def __init__(self, startPos, direction):
    	self.__position = np.array(startPos)
    	self.__vertices = [np.array(startPos)]
    	self. __direction = self.normalise(direction)

    def __repr__(self):
        return "vertices = %s, direction = %s" % (self.vertices(), self.direction())

    def position(self):
    	"""
    	Returns the current position vector of the ray
    	"""
    	return self.__position

    def vertices(self):
    	"""
    	Returns a list of the points at which the ray has changed direction
    	"""
    	return self.__vertices

    def direction(self):
    	"""
    	Returns the current direction vector of the ray
    	"""
    	return self.__direction

    def normalise(self,direction):
    	"""
    	Normalises a vector
    	"""
	magnitude = (np.dot(direction,direction))**0.5
	return np.array(direction)/magnitude

    def append(self, position, direction):
    	"""
    	Appends a new vertex onto the end of the vertices list and updates the current position and direction
    	"""
    	self.__position = position
    	self.__vertices.append(position)
    	self.__direction = self.normalise(direction)

class Bundle():
    """
    Class defining a bundle of Rays

    Note: Rays in bundle all start at z=0, may cause issues with circularity for bundle non-parallel to z-axis
    """

    def __init__(self, direction, radius, ringnumber, ringdensity):
        """
        Initialises a bundle of Rays  with the same direction vector by uniformly distributing them over a disc
        """
        self.__rays = []
        for ringRadius, theta in self._rtuniform(ringnumber, radius, ringdensity):
            for t in theta:
                self.__rays += [Ray([ringRadius * np.cos(t),ringRadius * np.sin(t),0], direction)]

    def rays(self):
        return self.__rays
    
    def __repr__(self):
        string = ""
        for ray in self.rays():
            string += ray.__repr__() + "\n"
        return string

    def _rtuniform(self, n, rmax, m):
        """
        Generator returning coordinates building up a uniformly covered disk ring by ring
    
        Inputs:
          n : number of rings making up the disk
          rmax : radius of the disk
          m : density of dots in each ring
              number of dots (n) in the ith ring:
              n = 1 for i = 0
              n = mi for i > 0
        Returns:
          r : A radius from R for the ring
          t : A list of arguments corresponding to the evenly spaced coordinates around the ring
        """

        ringradii = np.arange(0, rmax, rmax/n)
        mi = range(0, m*n, m)
        mi[0] = 1
        for r in ringradii:
            if r == 0:
                mi = 1
            else:
                mi = m * ringradii.tolist().index(r)
            t = np.arange(0,2*np.pi,2*np.pi/mi)
            yield r, t
