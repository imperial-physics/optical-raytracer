Year 2 computing labs project "An Optical Ray Tracer"
Tom French

Project Specification:
https://bb.imperial.ac.uk/bbcswebdav/pid-698942-dt-content-rid-2526955_1/courses/COURSE-PHY2_LAB-15_16/Y2Computing/Y2PyCourse/Students/Projects/html/Raytrace.html#optical-rays

**Tasks:**
Completed:

* Correct refraction from multiple convex surfaces
* Use of genpolar to generate "bundles" of rays.
* Iterating propagation over multiple surfaces automatically
* Some form of plotting the rays

Incomplete:

* Total internal reflection
* Testing on concave surfaces
* Refraction on planar surfaces

Needed:


Extensions:

* Reflection
* Dispersion
* Rainbows