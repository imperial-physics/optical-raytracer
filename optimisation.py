import scipy.optimize as opt
import numpy as np
import opticalElement
import raytracer
import rayPlotting

def calcRMSR(bundle):
	"""
	Calculates the RMS of the bundle's radius at the optical plane
	"""
	squaredRadius = 0
	for ray in bundle.rays():
		squaredRadius += ray.position()[0]**2 + ray.position()[1]**2

	RMSR = (squaredRadius / len(bundle.rays()))**0.5
	return RMSR

def repeatExp(rads, opticaldist, bundlerad):
	"""
	Function minimised by optimisecurvatures. Takes rads as the variable argument and returns the root mean square radius of the image at the optical plane
	"""
	bundle = runExp(opticaldist,rads,bundlerad)
	x = calcRMSR(bundle)
	return x

def repeatExpfocus(opticaldist, rads, bundlerad):
	"""
	Function minimised by findfocus. Takes opticaldist as the variable argument and returns the root mean square radius of the image at the optical plane
	"""

	bundle = runExp(opticaldist,rads,bundlerad)
	x = calcRMSR(bundle)
	return x

def runExp(opticaldist, rads, bundlerad):
	"""
	Function to set up an arrangement of a single lens with radii of curvature given by rads.
	A Bundle of radius bundlerad will be placed on the optical axis and propagated until it reaches an optical plane at opticaldist
	"""
	radCurvature1 = rads[0]
	radCurvature2 = rads[1]
	opticalElement.OpticalElement.elements = []
	bundle = raytracer.Bundle([0,0,1],bundlerad,10,6)
	
	lensZstart = 20
	lensseparation = 5
	aperture = 12
	"""
	if radCurvature1 == 0:
		aperture = abs(radCurvature2)
	elif radCurvature2 == 0:
		aperture = radCurvature1
	else:
	    aperture = min(radCurvature1, abs(radCurvature2))
	
	if radCurvature1 != 0:
	    offset1 = (radCurvature1**2 - aperture**2)**0.5
	else:
		offset1 = 0
	if radCurvature2 != 0:
	    offset2 = (radCurvature2**2 - aperture**2)**0.5
	else:
		offset2 = 0
    """
	if radCurvature1 != 0:
	    sphere1 = opticalElement.SphericalRefraction(lensZstart,radCurvature1,aperture,1,1.5168)
	else:
		plane1 = opticalElement.PlanarRefraction(lensZstart,[0,0,1],1,1.5168)
	if radCurvature2 != 0:
		sphere2 = opticalElement.SphericalRefraction(lensZstart+lensseparation,radCurvature2,aperture,1.5168,1)
	else:
		plane2 = opticalElement.PlanarRefraction(lensZstart+lensseparation,[0,0,1],1.5168,1)
	opticalPlane = opticalElement.OpticalPlane(opticaldist)


	opticalElement.propagate(bundle)
	return bundle

def findfocus(rad1, rad2, opticaldist, bundlerad = 0.1):
    """
    Optimisation function to find the focal length of a lens of given radii of curvature.
    Takes rad1 and rad2 as the curvatures of each surface of the lens.
    Takes opticaldist as the initial estimate of the optical plane location
    Takes bundlerad as the radius of the Bundle propagated though the lens.
    """
    rads = (rad1, rad2)
    x = opt.fmin_tnc(repeatExpfocus, opticaldist, args=(rads, bundlerad), bounds=[(0,None)], approx_grad = 1)
    #optbundle = runExp(x[0][0], rads, bundlerad)
    #rayPlotting.plotBundle()
    #rayPlotting.traceBundle(optbundle, "r")
    #rayPlotting.raypoints(optbundle,"ro")
    #rayPlotting.raypoints(optbundle,"ro", point = "focus")
    #rayPlotting.show()
    return x


def optimisecurvatures(rad1,rad2, opticaldist, bundlerad = 0.1):
    """
    Optimisation function to find the radii of curvature of a lens for a given focal distance.
    Takes rad1 and rad2 as the curvatures of each surface of the lens.
    Takes opticaldist as the initial estimate of the optical plane location
    Takes bundlerad as the radius of the Bundle propagated though the lens.
    """
    rads = (rad1, rad2)
    x = opt.fmin_tnc(repeatExp, rads, args = (opticaldist, bundlerad), bounds=[(0.001,None),(None,-0.001)],approx_grad = 1)
    optbundle = runExp(opticaldist, (x[0][0], x[0][1]), bundlerad)
    #rayPlotting.plotBundle()
    #rayPlotting.traceBundle(optbundle, "r")
    #rayPlotting.raypoints(optbundle,"ro")
    #rayPlotting.raypoints(optbundle,"ro", point = "focus")
    #rayPlotting.show()
    return x

