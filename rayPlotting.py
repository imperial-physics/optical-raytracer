import matplotlib.pyplot as plt

def plotBundle():
    global raytrace
    global start1
    global start2
    global focus1
    global focus2
    fig = plt.figure(figsize=(16, 8))
    gs = plt.GridSpec(2, 4)

    raytrace = fig.add_subplot(gs[0:2, 0:2])
    raytrace.set_title("Ray trace")
    raytrace.set_xlabel("Z - coordinate")
    raytrace.set_ylabel("X - coordinate")

    start1 = fig.add_subplot(gs[0, 2])
    start1.set_title("Z = 0")
    start1.set_xlabel("X - coordinate")
    start1.set_ylabel("Y - coordinate")

    start2 = fig.add_subplot(gs[0, 3])
    start2.set_title("Z = 0")
    start2.set_xlabel("X - coordinate")
    start2.set_ylabel("Y - coordinate")

    focus1 = fig.add_subplot(gs[1, 2])
    focus1.set_title("At Focus")
    focus1.set_xlabel("X - coordinate")
    focus1.set_ylabel("Y - coordinate")

    focus2 = fig.add_subplot(gs[1, 3])
    focus2.set_title("At Focus")
    focus2.set_xlabel("X - coordinate")
    focus2.set_ylabel("Y - coordinate")

    fig.subplots_adjust(left=0.05, bottom=0.08, right=0.98, top=0.94,
                        wspace=0.23, hspace=0.28)


def traceline(ray, colour):
    global raytrace
    xpoints = []
    zpoints = []
    for position in ray.vertices():
	xpoints.append(position[0])
	zpoints.append(position[2])

    raytrace.plot(zpoints, xpoints, color = colour)

def traceBundle(bundle, colour):
    for ray in bundle.rays():
        traceline(ray, colour)

def raypoints(bundle,  color, point = "start", plot = 1):
    """
    Collects the points at which the rays in a bundle intersect with either the z=0 plane
    or the plane at which the bundle is focused.
    """
    global start1
    global start2
    global focus1
    global focus2
    if plot != 1 and plot != 2:
        raise Exception("Please enter 1 or 2 as possible values of plot")
    for ray in bundle.rays():
        if point == "start":
            if plot == 1:
                start1.plot(ray.vertices()[0][0],ray.vertices()[0][1], color, ms = 4)
            else:
                start2.plot(ray.vertices()[0][0],ray.vertices()[0][1], color, ms = 4)
        elif point == "focus":
            if plot == 1:
                focus1.plot(ray.vertices()[-1][0],ray.vertices()[-1][1], color, ms = 4)
            else:
                focus2.plot(ray.vertices()[-1][0],ray.vertices()[-1][1], color, ms = 4)

def show():
    plt.show()